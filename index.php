
<?php
  if(isset($_GET['team'])) {
      $team = $_GET['team'];
  }
?>
  <!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <script src='https://code.jquery.com/jquery-3.2.1.min.js'></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.blue_grey-blue.min.css" />
      <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
      <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
      <link rel="stylesheet" href="./style.css">
      <title>Choisis ta pizza !</title>
    </head>
    <header>
      <h2><?php echo $team;?></h2>
    </header>
    <body>
      <p>Choisis les pizzas de ton équipe ainsi que le nombre nécéssaire, nous viendrons ensuite à toi pour le paiment.</p>
      <form id='form' action='commande.php?team=<?php echo $team; ?>' method="post">
        Nom <input type="text" required name=nom>
        Prenom <input type="text" required name=prenom>
        
                     <SELECT id="choixpizza" class="select" name="deroulant_pizza[]" size="1" required>


            <option selected="true" disabled="disabled" value="">Choisir pizza</option>
            <option value="Raclette & jambon de Savoie">Raclette & jambon de Savoie</option>
            <option value="Spicy Dallas Burger">Spicy Dallas Burger</option>
            <option value="Figue Chèvre">Figue Chèvre</option>
            <option value="Saumon D'écosse">Saumon D'écosse</option>
            <option value="Authentique Raclette">Authentique Raclette</option>
            <option value="Savoyarde">Savoyarde</option>
            <option value="Street Kebab">Street Kebab</option>
            <option value="4 Fromages">4 Fromages</option>
            <option value=" vaganzaa">Extravaganzza</option>
            <option value="Cannibale">Cannibale</option>
            <option value="Hypnotika">Hypnotika</option>
            <option value="Bacon Groovy">Bacon Groovy</option>
            <option value="Chickenita Pepperoni">Chickenita Pepperoni</option>
            <option value="Indienne">Indienne</option>
            <option value="Forestiere">Forestiere</option>
            <option value="Hawaienne Jambon">Hawaienne Jambon</option>
            <option value="Hawaienne Poulet">Hawaienne Poulet</option>
            <option value="Pepper beef">Pepper beef</option>
            <option value="Deluxe">Deluxe</option>
            <option value="Reine">Reine</option>
            <option value="Steak & Cheese">Steak & Cheese</option>
            <option value="Orientale">Orientale</option>
            <option value="Peppina">Peppina</option>
            <option value="Pêcheur">Pêcheur</option>
            <option value="Classique Jambon">Classique Jambon</option>
            <option value="Margherita">Margherita</option>
            <option value="Originale pepperoni">Originale pepperoni</option>
            <option value="Spéciale Merguez">Spéciale Merguez</option>
            </select>

          <input id="howmuch" class="number" type="number" min="1" max="10" name="howmuch[]" value="0" >


      </form>
      <input type="submit" form="form" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect"/>
      <button id='click'class="mdl-button mdl-js-button mdl-button--fab mdl-button--colored">+</button>
      <button id='remove'class="mdl-button mdl-js-button mdl-button--fab mdl-button--colored">-</button>

    </body>
    <script>

      $('#click').click(function() {

        $('.select').last().clone().appendTo($('#form')).find("#choixpizza").attr("name",function(i,oldVal) {
            return oldVal.replace(/\[(\d+)\]/,function(_,m){
                return "[" + (+m + 1) + "]";
            });
        });
        $('.number').last().clone().appendTo($('#form')).find("#howmuch").attr("name",function(i,oldVal) {
            return oldVal.replace(/\[(\d+)\]/,function(_,m){
                return "[" + (+m + 1) + "]";
            });
        });
        $('.number').last().val(0);
      });

      $('#remove').click(function() {
        if($('.select').length >= 2){
        $('.select').last().remove();
        $('.number').last().remove();
      }
      });

    </script>

  </html>
