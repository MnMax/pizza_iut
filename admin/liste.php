<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <script src='https://code.jquery.com/jquery-3.2.1.min.js'></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="./master.css">
    <title>Récap des commandes</title>
  </head>
  <header>
    <h1>Commandes</h1>
  </header>
  <body>
    <form class="monform" action="liste.php" method="post">

    <?php
    $host    = "localhost";
    $user    = "root";
    $pass    = "root";
    $db_name = "bdd_pizzas";


    $connection = mysqli_connect($host, $user, $pass, $db_name);


    if(mysqli_connect_errno()){
        die("connection failed: "
            . mysqli_connect_error()
            . " (" . mysqli_connect_errno()
            . ")");
    }
    $result = mysqli_query($connection,"SELECT Nom,Prenom,team,pizza,quantite,paye FROM pizzas ORDER BY team");
    $all_property = array();

    echo '<div class="container">';
    echo '<table id="data" class="table">
            <tr class="data-heading">';

    while ($property = mysqli_fetch_field($result)) {
      if($property->name != 'paye')
      {
        echo '<th>' . $property->name . '</th>';
        array_push($all_property, $property->name);
      }
      else {
        array_push($all_property, $property->name);
      }
    }
    echo '</tr>';


    while ($row = mysqli_fetch_array($result)) {
      foreach ($all_property as $item) {

        if ($item == 'paye' && $row[$item] == 0) {
          echo '<tr id="monid" class="dang">';
        }
        else if ($item == 'paye' && $row[$item] != 0){
          echo '<tr id="monid" class="highlight">';
        }
      }
        //echo '<tr id="monid" class="dang">';
        foreach ($all_property as $item) {
          if ($item != 'paye') {
            echo '<td>' . $row[$item] . '</td>';
          }
        }
        echo '</tr>';
    }
    echo "</table>";
    ?>
  </form>
  <button id='click'onclick="location.reload();">refresh</button>
  <script>


   $("#data tr").click(function() {

     $(this).toggleClass('highlight dang');
     if ($(this).hasClass('highlight')) {
       $.get(
           "payed.php",
           {paramOne : $(this).html()},
       );

     }
     else {
       $.get(
           "unpayed.php",
           {paramOne : $(this).html()},
       );
     }
});
  </script>
  </body>
  <div id="footer">
  </div>
</html>
